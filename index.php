<?php
mb_internal_encoding("UTF-8");
session_start();

function autoloadFunction($className){
	if(preg_match('/Controller$/', $className)){
		require("controllers/" . $className . ".php");
	} else{
		require("models/" . $className . ".php");
	}
}
spl_autoload_register("autoloadFunction");

$router = new RouterController();
$router->processURL(array($_SERVER['REQUEST_URI']));
$router->renderView();