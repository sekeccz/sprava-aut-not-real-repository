<?php
class LoginManager{
	
	public function logIn($userName, $password, $srcPage){
		$data = array($userName,$password);
		$result = Db::logIn("select id from accounts where username = ? and password = ?", $data);
		if($result[0] == 1){
			$data2 = array(date("Y-m-d"),$result[1]);
			Db::send("update accounts set last_login_date = ? where id = ?", $data2);
			$_SESSION['userId'] = $result[1];
			$_SESSION['enKey'] = "020123";
			return "homeboard";
		} else {
			switch($srcPage){
				case "home":
					return "home?lerror=1";	
					break;
				case "register":
					return "register?lerror=1";	
					break;
				case "demo":
					return "demo?lerror=1";
					break;
				case "faq":
					return "faq?lerror=1";
					break;
			}
		}
	}
}