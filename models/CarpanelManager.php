<?php

class CarpanelManager{
    
    public function getCarList(){
        $carIds = Car::getMyCarsIds();
        $questionMarks = Car::getQuestionMarks($carIds);
        return Db::fetchAll("select v.id, b.name, v.model from vehicles v left join brands b on v.brand = b.id where v.id in ($questionMarks) order by v.archive asc", $carIds);
    }
    
     public function dataRequest($carId){
        $data = Db::fetchOne("select v.year, v.engine, v.kw_power, f.name, v.archive, a.nickname, a.username, "
                    . "(select sum(s.price) from services s where s.vehicle_id = v.id) as 'money_service', "
                    . "v.fuel_consumption, v.id from vehicles v left join brands b on v.brand = b.id left join fuel_types f on v.fuel_type = f.id left join accounts a on v.account_id = a.id where v.id = :car group by v.id", array(':car'=>$carId));
        
        
        if(!isset($data[1])){$data[1]="Data nebyla zadána";}
        if(!isset($data[2])){$data[2]="Data nebyla zadána";}else{$data[2].="kW / ".round($data[2] * 1.35962)."PS";;}
        if($data[4] == 1){$data[4]="Archivováno";}else{$data[4]="Používá se";}
        if($data[5] != ""){$data[6]=$data[5];}
        $data[7] = number_format($data[7], 0, ',', ' ') . " Kč";
        $data[8] .= " l/100km";
        $data[10] = number_format(Car::getCarDistance($carId, 1), 0, ',', ' ') . " km";
        $data[11] = in_array($data[9], Car::getMyOwnCarsIds());
        $data[12] = $this->getSharedUsers($carId);
        
        $data[9] = Secure::encode($data[9]);
        
        return $data;
    }
    
    public function createShare($user, $carId){
        if(filter_var($user, FILTER_VALIDATE_EMAIL)) {
            $user = Db::fetchOne("select id from accounts where email = ?", array($user))[0];
        }
        $doExists = Db::check("select id from accounts where id = ?", array($user));
        if($doExists == 1){
            $isShared = Db::check("select id from userXvehicles where account_id = ? and vehicle_id = ?", array($user, $carId));
            if($isShared == 0){
                Db::send("insert into userXvehicles (account_id, vehicle_id) values (?, ?)", array($user, $carId));
                return $this->getSharedUsers($carId); //success
            } else {
                return 2; //car is already assign
            }
        } else {
            return 1; //the user doesn't exits
        }
    }
    
    public function giveCar($user, $carId){
        if(filter_var($user, FILTER_VALIDATE_EMAIL)) {
            $user = Db::fetchOne("select id from accounts where email = ?", array($user))[0];
        }
        $doExists = Db::check("select id from accounts where id = ?", array($user));
        if($user == User::getUserData()['id']){
            return 3;
        }
        if($doExists == 1){
            //do the giving
            $dataCar = Db::fetchOne("select * from vehicles where id = ?", array($carId));
            $nextId = Db::fetchOne("select max(id) from vehicles")[0];
            $nextId++;
            Db::send("insert into vehicles (id, account_id, brand, model, year, engine, kw_power, archive, fuel_type) values ($nextId, $user, $dataCar[2], '$dataCar[3]', $dataCar[4], '$dataCar[5]', $dataCar[6], 0, $dataCar[8])");
            $query = "insert into services (account_id, vehicle_id, tachometer, description, price, service_date) values ";
            $dataService = Db::fetchAll("select * from services where vehicle_id = ?", array($carId));
            if(!$dataService == array()){
                foreach($dataService as $data){
                    $query .= "($user, $nextId, $data[3], '$data[4]\r\nCena: $data[5]Kč', 0, '$data[6]'), ";
                }
                $query = substr($query, 0, -2);
                $query .= ";";
                Db::send($query);
            }
            $this->archiveCar($carId);
            return 2; //done
        } else {
            return 1; //the user doesn't exits
        }
    }
    
    public function editCar($carId, $mfgdt, $engine, $power){
        return Db::send("update vehicles SET year = ?, engine = ?, kw_power = ? WHERE id = ?", array($mfgdt, $engine, $power, $carId));
    }
    
    public function deleteShare($id){
        Db::send("delete from userXvehicles where id = ?", array($id));
        return $id;
    }
    
    public function archiveCar($carId){
        return Db::send("update vehicles SET archive = 1 WHERE id = ?", array($carId));
    }
    
    public function createLink($carId){
        $carId = Secure::encode($carId);
        return "https://car.renkei.cz/home/".$this->generateLetters().$carId.$this->generateLetters();
    }
    
    private function generateLetters($length = 3) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
    
    private function getSharedUsers($carId){ 
        $returnArray = Db::fetchAll("select a.username, a.nickname, uv.id, uv.account_id from userXvehicles uv left join accounts a on uv.account_id = a.id where uv.vehicle_id = ? and uv.account_id != ? order by uv.id asc", array($carId, User::getUserData()['id']));
        for($i = 0; $i < count($returnArray); $i++){
            if(!empty($returnArray[$i]['nickname'])){
                $returnArray[$i]['username'] = $returnArray[$i]['nickname'];
                $returnArray[$i][0] = $returnArray[$i]['nickname'];
            }
            if(empty($returnArray[$i]['username'])){
                $returnArray[$i]['username'] = "Ghost";
                $returnArray[$i][0] = "Ghost";
                $returnArray[$i]['account_id'] = 0;
                $returnArray[$i][3] = 0;
            }
        }
        
        if($returnArray == []){
            return false;
        }
        
        return $returnArray;        
    }
}
