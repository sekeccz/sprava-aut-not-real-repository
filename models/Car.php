<?php

class Car {
    
    public function __construct() {
        $this->userData = User::getUserData()[1];
    }

    public static function getMyCars($parameter) {
        $questionMarks = self::getQuestionMarks($parameter);
        return Db::fetchAll("select v.id, b.name, v.model from vehicles v left join brands b on v.brand = b.id where v.id in ($questionMarks) order by v.id", $parameter);
    }

    public static function getCarBrands() {
        return Db::fetchAll("select * from brands");
    }

    public static function getFuelList() {
        return Db::fetchAll("select * from fuel_types");
    }

    public static function getMyActiveCarsIds() {
        $ids = array();
        foreach(Db::getUser()[1] as $data){
            if(!$data['archive']){
                array_push($ids, array($data[0]));
            }
        }
        return self::putIdsToArray($ids);
    }
    
    public static function getMyCarsIds() {
        $ids = array();
        foreach(Db::getUser()[1] as $data){
            array_push($ids, array($data[0]));
        }
        return self::putIdsToArray($ids);
    }

    public static function getMyOwnActiveCarsIds() {
        $ids = array();
        foreach(Db::getUser()[1] as $data){
            if($data['is_owner'] && !$data['archive']){
                array_push($ids, array($data[0]));
            }
        }
        return self::putIdsToArray($ids);
    }
    
    public static function getMyOwnCarsIds() {
        $ids = array();
        foreach(Db::getUser()[1] as $data){
            if($data['is_owner']){
                array_push($ids, array($data[0]));
            }
        }
        return self::putIdsToArray($ids);
    }

    public static function getQuestionMarks($carIds) {
        $stringOfQuestionMarks = "";
        for ($i = 0; $i < count($carIds); $i++) {
            $stringOfQuestionMarks .= "?,";
        }
        return substr_replace($stringOfQuestionMarks, "", -1);
    }
    
    public static function getCarDistance($carId, $mode, $date = 0){
        switch($mode){
            case 1:
                $data = Db::fetchOne("select max(tachometer), min(tachometer) from vehicles_tachometer where vehicle_id = ?", array($carId));
                return $data[0]-$data[1];
                break;
            case 2:
                $nData = Db::fetchOne("select max(tachometer), min(tachometer), max(date), min(date) from vehicles_tachometer where vehicle_id = :car and year(date) = :date;", array(':date'=>$date, ':car'=>$carId));
                $aData = Db::fetchOne("select max(tachometer), min(tachometer), max(date), min(date) from vehicles_tachometer where vehicle_id = :car and year(date) = :date;", array(':date'=>($date+1), ':car'=>$carId));
                $bData = Db::fetchOne("select max(tachometer), min(tachometer), max(date), min(date) from vehicles_tachometer where vehicle_id = :car and year(date) = :date;", array(':date'=>($date-1), ':car'=>$carId));
                $date1 = new DateTime($nData[3]);
                $date2 = new DateTime($bData[2]);
                $date3 = new DateTime($aData[3]);
                $date4 = new DateTime($nData[2]);
                $date5 = new DateTime($date2->format('Y')."-12-31");
                $date6 = new DateTime($date1->format('Y')."-01-01");
                $date7 = new DateTime($date4->format('Y')."-12-31");
                $date8 = new DateTime($date3->format('Y')."-01-01");
                
                if(is_null($aData[0])){
                    $finalEndKm = $nData['max(tachometer)'];
                } else {
                    //Počet dnů, které zbývají do konce roku
                    $endDayDiffYear1 = $date7->diff($date4)->format("%a");
                    //Počet dnů navíc k dalšímu roku
                    $endDayDiffYear2 = $date3->diff($date8)->format("%a");
                    //Celkový počet dnů chybějící mezi zápisy ke konci roku
                    $endDayDiff = $endDayDiffYear1+$endDayDiffYear2;
                    if($endDayDiff == 0){
                        $finalEndKm = $nData[0];
                    } else {
                        //Celkový rozdíl kilometrů mezi zápisy ke konci roku
                        $kmEndDiff = $aData[1]-$nData[0];
                        //Kilometry za den v průměru ke konci roku
                        $kmEndDiffDay = $kmEndDiff/$endDayDiff;
                        //Kolik kilometrů přidat ke konci roku
                        $kmEndAdd = $kmEndDiffDay*$endDayDiffYear1;
                        //Stav tachometru ke konci roku
                        $finalEndKm = $nData[0]+$kmEndAdd;                        
                    }
                }
                
                if(is_null($bData[0])){
                    $finalStartKm = $nData['min(tachometer)'];
                } else {
                    //Počet dnů, které jsou navíc od začátku roku
                    $startDayDiffYear1 = $date1->diff($date6)->format("%a");
                    //Počet dnů zbávající k dalšímu roku
                    $startDayDiffYear2 = $date2->diff($date5)->format("%a");
                    //Celkový počet dnů chybějící mezi zápisy na začátku roku
                    $startDayDiff = $startDayDiffYear1+$startDayDiffYear2;
                    if($startDayDiff == 0){
                        $finalStartKm = $nData[1];
                    } else {
                        //Kilometry za den v průměru na začátku roku
                        $kmStartDiff = $nData[1]-$bData[0];
                        //Kilometry za den v průměru na začátku roku
                        $kmStartDiffDay = $kmStartDiff/$startDayDiff;
                        //Kolik kilometrů přidat k začátku roku
                        $kmStartAdd = $kmStartDiffDay*$startDayDiffYear1;
                        //Stav tachometru na začátku roku
                        $finalStartKm = $nData[1]-$kmStartAdd;
                    }
                }
                
                //Rozdíl mezi začátek roku a koncem roku
                $result = $finalEndKm - $finalStartKm;
                return $result;
                break;
            case 3:
                $nData = Db::fetchOne("select max(tachometer), min(tachometer), max(date), min(date) from vehicles_tachometer where vehicle_id = :car and year(date) = :date and month(date) = month(:date);", array(':date'=>$date, ':car'=>$carId));
                
                $baseDate = new DateTime($date);
                $futureDate = $baseDate->format('Y-m')."-31";
                $previousDate = $baseDate->format('Y-m')."-01";
                
                $aData = Db::fetchAll("select tachometer, date from vehicles_tachometer where vehicle_id = :car and date > :date order by date asc limit 1;", array(':date'=>$futureDate, ':car'=>$carId));
                $bData = Db::fetchAll("select tachometer, date from vehicles_tachometer where vehicle_id = :car and date < :date order by date desc limit 1;", array(':date'=>$previousDate, ':car'=>$carId));                
                
                /*echo "Další měsíc: "; print_r($aData[0]); echo "<br>";
                echo "Aktuální měsíc: "; print_r($nData); echo "<br>";
                echo "Minulý měsíc: "; print_r($bData[0]); echo "<br>";*/
                
                if(is_null($nData[0])){
                    return 0;
                }
                
                if($aData != array()){
                    $aData = $aData[0];
                } else {
                    $aData[0] = $nData[0];
                    $aData[1] = $nData[2];
                }
                
                if($bData != array()){
                    $bData = $bData[0];
                } else {
                    $bData[0] = $nData[1];
                    $bData[1] = $nData[3];
                }
                
                $date1 = new DateTime($nData[3]);
                $date2 = new DateTime($bData[1]);
                $date3 = new DateTime($aData[1]);
                $date4 = new DateTime($nData[2]);
                $date5 = new DateTime($date2->format('Y-m')."-31");
                $date6 = new DateTime($date1->format('Y-m')."-01");
                $date7 = new DateTime($date4->format('Y-m')."-31");
                $date8 = new DateTime($date3->format('Y-m')."-01");
                
                if(is_null($aData[0])){
                    $finalEndKm = $nData['max(tachometer)'];
                } else {
                    //Počet dnů, které zbývají do konce měsíce
                    $endDayDiffYear1 = $date4->diff($date7)->format("%a");
                    //Počet dnů navíc k dalšímu měsíci
                    $endDayDiffYear2 = $date3->diff($date7)->format("%a");
                    //Celkový počet dnů chybějící mezi zápisy ke konci měsíce
                    $endDayDiff = $endDayDiffYear1+$endDayDiffYear2;
                    if($endDayDiff == 0){
                        $finalEndKm = $nData[0];
                    } else {
                        //Celkový rozdíl kilometrů mezi zápisy ke konci měsíce
                        $kmEndDiff = $aData[0]-$nData[0];
                        //Kilometry za den v průměru ke konci měsíce
                        $kmEndDiffDay = $kmEndDiff/$endDayDiff;
                        //Kolik kilometrů přidat ke konci měsíce
                        $kmEndAdd = $kmEndDiffDay*$endDayDiffYear1;
                        //Stav tachometru ke konci měsíce
                        $finalEndKm = $nData[0]+$kmEndAdd;
                    }
                    
                }
                
                
                
                if(is_null($bData[0])){
                    $finalStartKm = $nData['min(tachometer)'];
                } else {
                    //Počet dnů, které jsou navíc od začátku měsíce
                    $startDayDiffYear1 = $date1->diff($date6)->format("%a");
                    //Počet dnů zbávající k dalšímu měsíce
                    $startDayDiffYear2 = $date2->diff($date6)->format("%a");
                    //Celkový počet dnů chybějící mezi zápisy na začátku měsíce
                    $startDayDiff = $startDayDiffYear1+$startDayDiffYear2;
                    if($startDayDiff == 0){
                        $finalStartKm = $nData[1];
                    } else {
                        //Kilometry za den v průměru na začátku měsíce
                        $kmStartDiff = $nData[1]-$bData[0];
                        //Kilometry za den v průměru na začátku měsíce
                        $kmStartDiffDay = $kmStartDiff/$startDayDiff;
                        //Kolik kilometrů přidat k začátku měsíce
                        $kmStartAdd = $kmStartDiffDay*$startDayDiffYear1;
                        //Stav tachometru na začátku měsíce
                        $finalStartKm = $nData[1]-$kmStartAdd;
                    }
                }
                
                //Rozdíl mezi začátek měsíce a koncem měsíce
                $result = $finalEndKm - $finalStartKm;
                return $result;               
                break;
        }
    }
    
    public static function deleteCar($carId){
            if(in_array($carId, self::getMyOwnCarsIds())){
                Db::send("delete from vehicles where id = ?", array($carId));
            }
    }
    
    private static function putIdsToArray($rawIds) {
        $tempArray = array();
        foreach ($rawIds as $row) {
            array_push($tempArray, $row[0]);
        }
        if (empty($tempArray)) {
            array_push($tempArray, 0);
        }
        return $tempArray;
    }
}
