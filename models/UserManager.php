<?php
use PHPMailer\PHPMailer\PHPMailer;
require "PHPMailer.php";
require "SMTP.php";

class UserManager {

    public function sendingVerifyEmail($userId){
        $data = Db::fetchOne("select email, email_verify from accounts where id = ?", array($userId));
        if($data['email_verify'] == 1){
            return "profile";
        } else {
            $this::sendVerifyEmail($userId, $data['email']);
            return "profile?success=1";
        }
    }
    
    public function verifyEmail($userId){
        Db::send("update accounts set email_verify=1 where id=?", array($userId));
        return "user?success=1";   
    }
    
    public static function sendVerifyEmail($userId, $userEmail){
        $to = $userEmail;
        $subject = "Ověření vaši e-mailové adresy";
        $message = "
                <html>
                <head>
                <title>HTML email</title>
                </head>
                <body>
                <h2>Správa Aut</h2>
                <hr>
                <p>pro ověření klikněte zde: <a href='https://www.car.renkei.cz/user?ve=".$userId."'>www.car.renkei.cz</a></p>
                </body>
                </html>";

        $mail = new PHPMailer(true);
        $mail->IsSMTP(true);
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'ssl';
        $mail->Host = 'mailproxy.webglobe.com';
        $mail->Port = 465;
        $mail->Username = 'sprava-aut@renkei.cz';
        $mail->Password = 'pass';
        $mail->setFrom('sprava-aut@renkei.cz', 'Správa Aut');
        $mail->addAddress($to);
        $mail->isHTML(true);
        $mail->CharSet = 'utf-8';
        $mail->Subject = $subject;
        $mail->Body = $message;

        $mail->Send();
    }
}
