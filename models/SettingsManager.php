<?php

class SettingsManager {

    private $data;
    private $allSettings = array(
        "allCar", "allProfile", "last10Fuel", "fuelPriceGraph",
        "useTachometer", "writeFuel",
        "countFuel"
    );

    public function getAllSettings() {
        $this->downloadSettings();
        return $this->data;
    }

    public function getHomeboardData() {
        $this->downloadSettings();
        return $this->data[0];
    }

    public function isHomeboardEmpty() {
        $temp = 0;
        $this->downloadSettings();
        for ($i = 0; $i < count($this->data[0]) / 2; $i++) {
            if ($this->data[0][$i] == 1) {
                $temp = 1;
            }
        }
        return $temp;
    }

    public function getRideData() {
        $this->downloadSettings();
        return $this->data[1];
    }

    public function getFuelData() {
        $this->downloadSettings();
        return $this->data[2];
    }

    public function uploadSettings($post) {
        Db::send("update userXsettings SET settings = ? WHERE account_id = ?", array($this->encodeSettings($post), User::getUserData()['id']));
    }

    private function downloadSettings() {
        $rawSettings = Db::getUser()[2]['settings'];
        $this->data = $this->decodeSettings($rawSettings);
    }

    public function encodeSettings($array) {
        $rawSettings = "";
        $c = 0;
        $tempArray = array();

        for ($i = 0; $i < count($this->data); $i++) {
            $n = count($this->data[$i]) / 2;
            for ($y = 0; $y < $n; $y++) {
                if (isset($array[$this->allSettings[$c]]) && $array[$this->allSettings[$c]]) {
                    $tempArray[$i][$y] = 1;
                } else {
                    $tempArray[$i][$y] = 0;
                }
                $c++;
            }
        }

        for ($i = 0; $i < count($tempArray); $i++) {
            for ($y = 0; $y < count($tempArray[$i]); $y++) {
                $rawSettings .= $tempArray[$i][$y];
            }
            $rawSettings .= "K";
        }

        return($rawSettings);
    }

    private function decodeSettings($string) {
        $rawSegment = "";
        $rawArraySegment = [];
        $c = 0;

        $rawArraySegment = explode("K", $string);

        for ($i = 0; $i < count($rawArraySegment); $i++) {
            $tempArray = [];
            $n = strlen($rawArraySegment[$i]);
            for ($y = 0; $y < $n; $y++) {
                array_push($tempArray, $rawArraySegment[$i][$y]);
                $tempArray[$this->allSettings[$c]] = $rawArraySegment[$i][$y];
                $c++;
            }

            $arraySegment[$i] = $tempArray;
        }

        return $arraySegment;
    }

}
