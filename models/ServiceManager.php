<?php

class ServiceManager{
	
        public function getTable(){
                $fullArray = array();
                $carIds1 = Car::getMyCarsIds();
                $questionMarks1 = Car::getQuestionMarks($carIds1);
                for($i = 0; $i < count($carIds1); $i++){
                    array_push($fullArray, $carIds1[$i]);
                }
                array_push($fullArray, User::getUserData()['id']);
                $carIds2 = Car::getMyOwnCarsIds();
                $questionMarks2 = Car::getQuestionMarks($carIds2);
                for($i = 0; $i < count($carIds2); $i++){
                    array_push($fullArray, $carIds2[$i]);
                }
		return Db::fetchAll("select s.id, s.account_id, b.name as 'Brand', v.model as 'Model', date_format(s.service_date, '%d.%m.%Y') as 'Datum servisu', s.tachometer, s.description, s.price from services s inner join vehicles v on s.vehicle_id = v.id inner join brands b on v.brand = b.id where (s.vehicle_id in ($questionMarks1) and s.account_id = ?) or s.vehicle_id in ($questionMarks2) order by s.service_date desc", $fullArray);		
	}
	
	public function getService($parameter){
		return Db::fetchOne("select vehicle_id, tachometer, description, price, service_date from services where id = ?", array($parameter));
	}
        
        public function addService($driverId, $car, $tachometer, $price, $description, $serviceDate){
            Db::send("insert into services (account_id, vehicle_id, tachometer, price, description, service_date) values (?, ?, ?, ?, ?, ?)", array($driverId, $car, $tachometer, $price, $description, $serviceDate));
        }
        
        public function updateService($driverId, $car, $tachometer, $price, $description, $serviceDate, $serviceId){
            DB::send("update services set account_id=?, vehicle_id=?, tachometer=?, price=?, description=?, service_date=? where id=?", array($driverId, $car, $tachometer, $price, $description, $serviceDate, $serviceId));
        }
        
        public function deleteService($id){
            Db::send("delete from services where id = ?", array($id));
        }
}

