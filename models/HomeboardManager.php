<?php

class HomeboardManager {
    private $userData;
    
    public function __construct() {
        $this->userData = User::getUserData();
    }

    public function getBoxes($settings) {
        $return = array();
        for ($i = 0; $i < count($settings); $i += 2) {
            if ($settings[$i / 2] == 1) {
                $nameOfFunction = array_keys($settings)[$i + 1];
                array_push($return, $this->$nameOfFunction());
            }
        }
        return $return;
    }
    
    public function jAlgorithm(){
        $data_refueling = Db::fetchAll("select tachometer, vehicle_id, id, fuel_date from refueling order by fuel_date asc");
        $data_services = Db::fetchAll("select service_date, vehicle_id, id, tachometer from services order by service_date asc");
        $data_rides = DB::fetchAll("select tachometer_before, tachometer_after, ride_date, vehicle_id, id from rides order by ride_date asc");
        foreach ($data_refueling as $fuel){
            if($fuel['tachometer'] > 0){
                Db::send("insert into vehicles_tachometer (vehicle_id, tachometer, date, source) values (?, ?, ?, ?)", array($fuel['vehicle_id'], $fuel['tachometer'], $fuel['fuel_date'], "F".$fuel['id']));
            }
        }
        foreach ($data_services as $serv){
            if($serv['tachometer'] > 0){
                Db::send("insert into vehicles_tachometer (vehicle_id, tachometer, date, source) values (?, ?, ?, ?)", array($serv['vehicle_id'], $serv['tachometer'], $serv['service_date'], "S".$serv['id']));
            }
        }
        foreach ($data_rides as $ride){
            if($ride['tachometer_before'] > 0){
                Db::send("insert into vehicles_tachometer (vehicle_id, tachometer, date, source) values (?, ?, ?, ?)", array($ride['vehicle_id'], $ride['tachometer_before'], $ride['ride_date'], "RB".$ride['id']));
            }
            if($ride['tachometer_after'] > 0){
                Db::send("insert into vehicles_tachometer (vehicle_id, tachometer, date, source) values (?, ?, ?, ?)", array($ride['vehicle_id'], $ride['tachometer_after'], $ride['ride_date'], "RA".$ride['id']));
            }
        }
    }
    
    public function allCarRequest($carId, $date){
        if($date == 0){
            $data = Db::fetchOne("select b.name, v.model, "
                    . "(select sum(s.price) from services s where s.vehicle_id = v.id) as 'money_service', "
                    . "(select round(sum(r.price), 0) from refueling r where r.vehicle_id = v.id) as 'money_fuel', "
                    . "v.fuel_consumption, "
                    . "(select F_PricePerKilometer(v.fuel_consumption,(select avg(price_per_amount) "
                    . "from refueling where vehicle_id = :car))) as 'PpK' from vehicles v left join brands b on v.brand = b.id where v.id = :car group by v.id", array(':car'=>$carId));
            $data[6] = Car::getCarDistance($carId, 1);
        } elseif(strlen($date) == 4){
            $data = Db::fetchOne("select b.name, v.model, "
                    . "(select sum(s.price) from services s where s.vehicle_id = v.id  and year(s.service_date) = :date) as 'money_service', "
                    . "(select round(sum(r.price), 0) from refueling r where r.vehicle_id = v.id and year(r.fuel_date) = :date) as 'money_fuel', "
                    . "(select round(avg(f.fuel_consumption) * 1,1) from fuel_consumption f where f.vehicle_id = v.id and year(f.start_date) = :date) as 'fffuel_consumption', "
                    . "(select F_PricePerKilometer(fffuel_consumption,(select avg(price_per_amount) from refueling where vehicle_id = :car and year(fuel_date) = :date))) as 'PpK' "
                    . "from vehicles v left join brands b on v.brand = b.id where v.id = :car group by v.id", array(':date'=>$date, ':car'=>$carId));
            $data[6] = Car::getCarDistance($carId, 2, $date);
        } else {
            $data = Db::fetchOne("select b.name, v.model, "
                    . "(select sum(s.price) from services s where s.vehicle_id = v.id  and year(s.service_date) = :date and month(s.service_date) = month(:date)) as 'money_service', "
                    . "(select round(sum(r.price), 0) from refueling r where r.vehicle_id = v.id and year(r.fuel_date) = :date and month(r.fuel_date) = month(:date)) as 'money_fuel', "
                    . "(select round(avg(f.fuel_consumption) * 1,1) from fuel_consumption f where f.vehicle_id = v.id and year(f.start_date) = :date and month(f.start_date) = month(:date)) as 'ffuel_consumption', "
                    . "(select F_PricePerKilometer(ffuel_consumption,(select avg(price_per_amount) from refueling where vehicle_id = :car and year(fuel_date) = :date and month(fuel_date) = month(:date)))) as 'PpK' "
                    . "from vehicles v left join brands b on v.brand = b.id where v.id = :car group by v.id", array(':date'=>$date, ':car'=>$carId));
            $data[6] = Car::getCarDistance($carId, 3, $date);
        }
        
        for($i = 2; $i < 7; $i++){
            if($data[$i] == 0){
                $data[$i] = "Nedostatek dat";
            } else {
                switch($i){
                    case 2:
                        $data[$i] = number_format($data[$i], 0, ',', ' ') . " Kč";
                        break;
                    case 3:
                        $data[$i] = number_format($data[$i], 0, ',', ' ') . " Kč";
                        break;
                    case 4:
                        $data[$i] = $data[$i] . " l/100km";
                        break;
                    case 5:
                        $data[$i] = number_format($data[$i], 2, ',', ' ') . " Kč";
                        break;
                    case 6:
                        $data[$i] = number_format($data[$i], 0, ',', ' ') . " km";
                        break;
                } 
            }
        }
        
        return $data;
    }
    
    private function allCar() {
        $year = 3000;
        setlocale(LC_TIME, 'cs_CZ.UTF-8');
        $userStartDate = $this->userData['create_date'];
        $userStartDate = new DateTime($userStartDate);
        $userStartDate = $userStartDate->format('Y-m');
        $userStartDate .= date('-d');
        $months = new DatePeriod(new DateTime($userStartDate), DateInterval::createFromDateString('1 month'), new DateTime());
        $months = array_reverse(iterator_to_array($months));
        $carIds = Car::getMyCarsIds();
        $questionMarks = Car::getQuestionMarks($carIds);
        $cars = Db::fetchAll("select v.id, b.name, v.model from vehicles v left join brands b on v.brand = b.id where v.id in ($questionMarks) order by v.archive asc", $carIds);
        $resultData = "";
        $resultData .= "<div class='box profile_box'>
                        <h5>Souhrn aut - <select name='car' id='car' class='invisibleSelect'>";
        foreach ($cars as $data) {
            $resultData .= "<option value='".$data['id']."'>" . $data['name'] . " " . $data['model'] . "</option>";
        }
        $resultData .= "</select>
                        <a class='rightPosition'>Období: <select name='date' id='date' class='invisibleSelect'>
                        <option value=0>Celkem</option>";
        foreach ($months as $date) {
            if($date->format("Y") < $year){
                $year = $date->format("Y");
                $resultData .= "<option value='".$date->format("Y")."'>" . $date->format("Y") . "</option>";
                $resultData .= "<option value='".$date->format("Y-m-d")."'>" . strftime("%B %Y", strtotime($date->format("Y-m"))) . "</option>";
            } else {
                $resultData .= "<option value='".$date->format("Y-m-d")."'>" . strftime("%B %Y", strtotime($date->format("Y-m"))) . "</option>";
            }
        }
        $resultData .= "</select></a></h5>
                        <hr>
                        <div class='info_obal'>
                            <div class='info_box' style='min-width: 346.3px; width: -webkit-fill-available;'>
                                <strong>Ujeto kilometrů: </strong><a id='showKm'>Načítání dat</a><br>
                                <strong>Utraceno za servis: </strong><a id='showService'>Načítání dat</a><br>
                                <strong>Utraceno za palivo: </strong><a id='showFuel'>Načítání dat</a><br>
                                <strong>Průměrná spotřeba: </strong><a id='showFC'>Načítání dat</a><br>
                                <strong>Cena za kilometr: </strong><a id='showPpK'>Načítání dat</a>
                            </div>
                        </div>
                        </div>";
        $resultData .= "<script>
                        document.getElementById('car').onchange = function() {
                            Loading();
                            GetData();
                        };
                        
                        document.getElementById('date').onchange = function() {
                            Loading();
                            GetData();
                        };

                        document.addEventListener('DOMContentLoaded', function() {
                            GetData();
                        }, false);
                
                        function Loading(){
                            document.getElementById('showKm').innerHTML = 'Načítání dat';
                            document.getElementById('showService').innerHTML = 'Načítání dat';
                            document.getElementById('showFuel').innerHTML = 'Načítání dat';
                            document.getElementById('showFC').innerHTML = 'Načítání dat';
                            document.getElementById('showPpK').innerHTML = 'Načítání dat';
                        }

                        function GetData(){
                            let eDate = document.getElementById('date').value;
                            let eCar = document.getElementById('car').value;
                            $.ajax({
                                type : 'POST',
                                url : 'homeboard',
                                data : {
                                    iDate: eDate, 
                                    iCar: eCar
                                },
                                dataType : 'json',
                                success: function (data) {
                                    document.getElementById('showKm').innerHTML = data[6];
                                    document.getElementById('showService').innerHTML = data[2];
                                    document.getElementById('showFuel').innerHTML = data[3];
                                    document.getElementById('showFC').innerHTML = data[4];
                                    document.getElementById('showPpK').innerHTML = data[5];
                                },
                                error : function () {
                                    console.log ('error');
                                }
                            });
                        }
                        </script>";
        return $resultData;       
    }
    
    private function allProfile() {
        if($this->userData['nickname'] == ""){
            $nickname = $this->userData['username'];
        } else {
            $nickname = $this->userData['nickname'];
        }
        $resultData = "";
        $userId = $this->userData['id'];
        $resultData .= "<div class='box profile_box'>
				<h5>Souhrn profilu - ".$nickname."</h5>
				<hr>
				<div class='info_obal' style='scroll-snap-type: x mandatory'>";
        
        //Začátek jízd
        
        $ride1 = Db::fetchAll("select sum(ride_km) as allkm from rides where account_id = ?", array($userId));
        $ride2 = Db::fetchAll("select ride_km, date_format(ride_date, '%d.%m.%Y') as date from rides where account_id = ? order by ride_km desc limit 1", array($userId));
        $ride3 = Db::fetchAll("select b.name, v.model, sum(r.ride_km) as allkm from rides r inner join vehicles v on r.vehicle_id = v.id inner join brands b on v.brand = b.id where r.account_id = ? group by r.vehicle_id order by sum(r.ride_km) desc limit 1", array($userId));
        if($ride2 == array()){
            $ride1['allkm'] = 0;
            $ride2['ride_km'] = 0;
            $ride2['date'] = "00.00.0000";
            $ride3['name'] = "Nevyhodnoceno";
            $ride3['model'] = "";
            $ride3['allkm'] = 0;
        } else {
            $ride1 = $ride1[0];
            $ride2 = $ride2[0];
            $ride3 = $ride3[0];
        }
        $resultData .= "<div class='info_box' style='min-width: 346.3px; scroll-snap-align: center''>
                        <h5>Jízdy</h5>
			<hr>
                        <a><strong>Celkem ujeto kilometrů: </strong>" . number_format($ride1['allkm'], 0, ',', ' ') . " km</a><br>
                        <a><strong>Nejdelší cesta: </strong>" . number_format($ride2['ride_km'], 0, ',', ' ') . " km (".$ride2['date'].")</a><br>
                        <a><strong>Nejvíce ujeto s: </strong>" . $ride3['name']." ".$ride3['model'] . " (". number_format($ride3['allkm'], 0, ',', ' ') ." km)</a><br>
                        </div>";
        
        //Začátek servisu
        
        $service1 = Db::fetchAll("select sum(price) as price from services where account_id = ?", array($userId));
        $service2 = Db::fetchAll("select b.name, v.model, s.price, s.description from services s inner join vehicles v on s.vehicle_id = v.id inner join brands b on v.brand = b.id where s.account_id = ? order by s.price desc limit 1", array($userId));
        $service3 = Db::fetchAll("select count(s.id) as pocet, b.name, v.model from services s inner join vehicles v on s.vehicle_id = v.id inner join brands b on v.brand = b.id where s.account_id = ? group by s.vehicle_id order by count(s.id) desc limit 1", array($userId));
        $service4 = Db::fetchAll("select sum(s.price) as price, b.name, v.model from services s inner join vehicles v on s.vehicle_id = v.id inner join brands b on v.brand = b.id where s.account_id = ? group by s.vehicle_id order by sum(s.price) desc limit 1", array($userId));
        if($service2 == array()){
            $service1['price'] = 0;
            $service2['name'] = "Nevyhodnoceno";
            $service2['model'] = "";
            $service2['price'] = 0;
            $service2['description'] = "";
            $service3['name'] = "Nevyhodnoceno";
            $service3['model'] = "";
            $service3['pocet'] = 0;
            $service4['name'] = "Nevyhodnoceno";
            $service4['model'] = "";
            $service4['pocet'] = 0;
            $service4['price'] = 0;
        } else {
            $service1 = $service1[0];
            $service2 = $service2[0];
            $service3 = $service3[0];
            $service4 = $service4[0];
        }
        
        $resultData .= "<div class='info_box' style='min-width: 346.3px; scroll-snap-align: center''>
                        <h5>Servis</h5>
			<hr>
                        <a><strong>Celkem zaplaceno za servis: </strong>" . number_format($service1['price'], 0, ',', ' ') . " Kč</a><br>
                        <a><strong>Nejdražší servis za: </strong>" . number_format($service2['price'], 0, ',', ' ') . " Kč (".$service2['name']." ".$service2['model'].")</a><br>
                        <a><strong>Nejvíce servisovaný auto: </strong>" . $service3['name']." ".$service3['model']." (".$service3['pocet']."x)</a><br>
                        <a><strong>Nejdražší auto na servis: </strong>" . $service4['name']." ".$service4['model']." (".number_format($service4['price'], 0, ',', ' ')." Kč)</a><br>
                        </div>";
        
        //Začátek tankování   
        
        $fuel1 = Db::fetchAll("select round(sum(price), 0) as price from refueling where account_id = ?", array($userId));
        $fuel2 = Db::fetchAll("select f.name, round(avg(r.price_per_amount), 2) as price from refueling r inner join fuel_types f on r.fuel_type = f.id where r.account_id = ? group by f.name", array($userId));
        if(is_null($fuel1[0][0])){
            $fuel1['price'] = 0;
        } else {
            $fuel1 = $fuel1[0];
        }
        $resultData .= "<div class='info_box' style='min-width: 346.3px; scroll-snap-align: center''>
                        <h5>Tankování</h5>
			<hr>
                        <a><strong>Celkem zaplaceno na PHM: </strong>" . number_format($fuel1['price'], 0, ',', ' ') . " Kč</a><br>
                        <a><strong>Průměrná cena PHM: </strong>";
                            foreach($fuel2 as $data){
                                $resultData .= "<br>".$data[0]." => ".$data[1]." Kč";
                            }
         $resultData .= "</a><br>
                        </div>";
        
        $resultData .= "</div></div>";

        return $resultData;
    }
    
    private function last10Fuel() {
        $carIds = Car::getMyCarsIds();
        $questionMarks = Car::getQuestionMarks($carIds);
        $fuel = Db::fetchAll("select b.name, v.model, fc.fuel_consumption, r.price, r.fuel_amount, r.distance, date_format(r.fuel_date, '%d.%m.%Y') as 'date' from fuel_consumption fc right join refueling r on fc.refueling_id = r.id inner join vehicles v on r.vehicle_id = v.id inner join brands b on v.brand = b.id where r.vehicle_id in ($questionMarks) order by r.fuel_date desc, fc.id desc limit 10", $carIds);
        $resultData = "";
        $resultData .= "<div class='box profile_box'>
				<h5>Posledních 10 záznamů o tankování</h5>
				<hr>
				<div class='info_obal' style='scroll-snap-type: x mandatory'>";
        foreach ($fuel as $data) {
            $resultData .= "<div class='info_box' style='min-width: 220px; scroll-snap-align: center'>
					<center><h5>" . $data['date'] . "</h5></center>
					<hr>
					<center><a>" . $data['name'] . " " . $data['model'] . "</a></center>
					<br>
                                        <a style='float: left;'>Spotřeba</a>";
            $resultData .= "<a style='float: right;'>Tankování</a>
					<br>";
            if($data['distance'] != 0){$resultData .= "<a class='highlightedText' style='float: left; padding-right: 5%'>" . $data['fuel_consumption'] . " l/100km</a>";}	
            $resultData .= "<a class='highlightedText' style='float: right;'>" . $data['price'] . " Kč</a>
                                        <br>";
            if($data['distance'] != 0){$resultData .= "<a style='float: left;'>" . number_format($data['distance'], 0, ',', ' ') . " km</a>";}
            $resultData .= "<a style='float: right;'>" . $data['fuel_amount'] . " l</a>
				</div>";
        }
        $resultData .= "</div></div>";

        return $resultData;
    }

    private function fuelPriceGraph() {
        $carIds = Car::getMyCarsIds();
        $questionMarks = Car::getQuestionMarks($carIds);
        $fuelTypes = array();
        $resultData = "";
        $resultData .= "<div class='box profile_box'>
				<h5>Vývoj ceny PHM</h5>
				<hr>
				<div class='info_obal'>";
        $fuelData = Db::fetchAll("select fuel_type from refueling where vehicle_id in ($questionMarks) or account_id = ".$this->userData['id']." group by fuel_type", $carIds);
        foreach ($fuelData as $row) {
            array_push($fuelTypes, $row[0]);
        }
        foreach ($fuelTypes as $fuelType) {
            $priceArray = array();
            $dateArray = array();
            $priceString = "";
            $dateString = "";
            $carIds = Car::getMyCarsIds();
            array_push($carIds, $fuelType);
            $data = Db::fetchAll("select price_per_amount, date_format(fuel_date, '%d.%m.%Y') as 'date' from refueling where (vehicle_id in ($questionMarks) or account_id = ".$this->userData['id'].") and fuel_type = ?  order by fuel_date asc", $carIds);
            $fuelName = Db::fetchOne("select name from fuel_types where id = ?", array($fuelType))[0];
            foreach ($data as $price) {
                array_push($priceArray, $price['price_per_amount']);
                array_push($dateArray, "'" . $price['date'] . "'");
            }
            $priceString = implode(",", $priceArray);
            $dateString = implode(",", $dateArray);
            $resultData .= "<div class='info_box' style='min-width: 346.3px; height: auto'><canvas id='fuel" . $fuelType . "Chart' style='width:500px'></canvas></div>
                        <script>
                        var xValues = [" . $dateString . "];
                        var yValues = [" . $priceString . "];
                            

                        var backgroundColorStyle = 'rgba(33,150,243,1.0)';
                        var borderColorStyle = 'rgba(157,212,255,0.5)';
                        if(localStorage.getItem('theme') == 'black'){
                            console.log('something');
                            backgroundColorStyle = 'rgba(64,181,173,1.0)';
                            borderColorStyle = 'rgba(64,181,173,0.5)';
                        }

                        new Chart('fuel" . $fuelType . "Chart', {
                          type: 'line',
                          data: {
                            labels: xValues,
                            datasets: [{
                              fill: false,
                              lineTension: 0.2,
                              backgroundColor: backgroundColorStyle,
                              borderColor: borderColorStyle,
                              data: yValues
                            }]
                          },
                          options: {
                            legend: {display: false},
                            title: {
                                display: true,
                                text: '" . $fuelName . "',
                                fontSize: 16
                            },
                            scales: {
                              yAxes: [{ticks: {min: " . round(min($priceArray) - 1) . ", max: " . round(max($priceArray) + 1) . "}}],
                            }
                          }
                        });
                        </script>";
        }

        return $resultData;
    }

}
