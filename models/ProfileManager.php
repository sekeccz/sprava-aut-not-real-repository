<?php
class ProfileManager{
	
	public function updateNickname($nickname, $userId){
		Db::send("update accounts set nickname = ? where id = ?", array($nickname, $userId));
	}
        
        public function updateEmail($email, $userId){
		Db::send("update accounts set email = ? where id = ?", array($email, $userId));
                Db::send("update accounts set email_verify = 0 where id = ?", array($userId));
	}
        
        public function getCars($parameter){
            $questionMarks = Car::getQuestionMarks($parameter);
            $rows = Db::fetchAll("select v.id, b.name as bname, v.model, v.year, v.engine, v.kw_power, f.name as fname, v.account_id, v.archive from vehicles v left join brands b on v.brand = b.id left join fuel_types f on v.fuel_type = f.id where v.id in ($questionMarks) order by v.id", $parameter);
            for($i = 0; $i < count($rows); $i++){
                if(is_null($rows[$i]['engine'])){
                    $rows[$i]['engine'] = "Data nebyla zadána";
                }
                
                if(is_null($rows[$i]['kw_power'])){
                    $rows[$i]['kw_power'] = "Data nebyla zadána";
		} else {
                    $rows[$i]['kw_power'] .= "kW / ".round($rows[$i]['kw_power'] * 1.35962)."PS";
		}
            }
            return $rows;
        }
        
        public function createCar($userId, $brand, $model, $year, $engine, $kw_power, $fuel_type){
            if($kw_power == 0){
		$kw_power = NULL;
            }

            if($engine == ""){
                    $engine = NULL;
            }
            
            Db::send("insert into vehicles (account_id, brand, model, year, engine, kw_power, fuel_type) values (?, ?, ?, ?, ?, ?, ?)", array($userId, $brand, $model, $year, $engine, $kw_power, $fuel_type));
            return "profile";
        }
}

