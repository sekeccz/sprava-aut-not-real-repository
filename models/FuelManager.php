<?php

class FuelManager{
    	
    public function getTable(){
        $fullArray = array();
        $carIds1 = Car::getMyCarsIds();
        $questionMarks1 = Car::getQuestionMarks($carIds1);
        for($i = 0; $i < count($carIds1); $i++){
            array_push($fullArray, $carIds1[$i]);
        }
        array_push($fullArray, User::getUserData()['id']);
        $carIds2 = Car::getMyOwnCarsIds();
        $questionMarks2 = Car::getQuestionMarks($carIds2);
        for($i = 0; $i < count($carIds2); $i++){
            array_push($fullArray, $carIds2[$i]);
        }
	$rawData = Db::fetchAll("select f.id, f.account_id, b.name as 'Brand', v.model as 'Model', date_format(f.fuel_date, '%d.%m.%Y') as 'Datum tankování', f.fuel_amount, f.price_per_amount, fc.distance, f.price, concat(replace(format(f.tachometer, 0), ',', ' '), ' ') as 'tachometer' from refueling f inner join vehicles v on f.vehicle_id = v.id left join fuel_consumption fc on f.id = fc.refueling_id inner join brands b on v.brand = b.id where (f.vehicle_id in ($questionMarks1) and f.account_id = ?) or f.vehicle_id in ($questionMarks2) order by f.fuel_date desc, f.id desc", $fullArray);		
	
        $finalData = array();
        foreach($rawData as $data){
            $data[0] = Secure::encode($data[0]);
            $data[5] .= " l";           
            $data[6] .= " Kč";
            if($data[7] == 0){
                $data[7] = "Nejsou data";           
            } else {
                $data[7] .= " km";
            }
            $data[8] .= " Kč";
            $data[9] = User::getUserData()['id'];
            array_push($finalData, $data);
        }
        
        echo json_encode($finalData);
    }
        
    public function addFuel($driverId, $carId, $fuelDate, $tachoFuel, $kmFuel, $fuelAmount, $pricePerAmount, $mode){
        $price = $fuelAmount * $pricePerAmount;
        $fuelType = $this->getFuelType($carId);
        switch($mode){
            case 0:
                Db::send("insert into refueling (account_id, vehicle_id, price_per_amount, price, fuel_amount, fuel_type, fuel_date) values (?, ?, ?, ?, ?, ?, ?)", array($driverId, $carId, $pricePerAmount, $price, $fuelAmount, $fuelType, $fuelDate));
                break;
            case 1:
                if($kmFuel == ""){
                    Db::send("insert into refueling (account_id, vehicle_id, tachometer, price_per_amount, price, fuel_amount, fuel_type, fuel_date) values (?, ?, ?, ?, ?, ?, ?, ?)", array($driverId, $carId, $tachoFuel, $pricePerAmount, $price, $fuelAmount, $fuelType, $fuelDate));                    
                } else {
                    Db::send("insert into refueling (account_id, vehicle_id, distance, tachometer, price_per_amount, price, fuel_amount, fuel_type, fuel_date) values (?, ?, ?, ?, ?, ?, ?, ?, ?)", array($driverId, $carId, $kmFuel, $tachoFuel, $pricePerAmount, $price, $fuelAmount, $fuelType, $fuelDate));
                    $this->calcFuelConsumption($driverId, $carId, $fuelDate, $kmFuel, $fuelAmount);
                }
                break;
            case 2:
                $distance = $this->getDistance($carId, $tachoFuel);
                Db::send("insert into refueling (account_id, vehicle_id, distance, tachometer, price_per_amount, price, fuel_amount, fuel_type, fuel_date) values (?, ?, ?, ?, ?, ?, ?, ?, ?)", array($driverId, $carId, $distance, $tachoFuel, $pricePerAmount, $price, $fuelAmount, $fuelType, $fuelDate));                    
                $this->calcFuelConsumption($driverId, $carId, $fuelDate, $distance, $fuelAmount);
                break;
            case 3:
                Db::send("insert into refueling (account_id, vehicle_id, distance, price_per_amount, price, fuel_amount, fuel_type, fuel_date) values (?, ?, ?, ?, ?, ?, ?, ?)", array($driverId, $carId, $kmFuel, $pricePerAmount, $price, $fuelAmount, $fuelType, $fuelDate));
                $this->calcFuelConsumption($driverId, $carId, $fuelDate, $kmFuel, $fuelAmount);
                break;
        }
            
            
        $rawData = Db::fetchAll("select f.id, f.account_id, b.name as 'Brand', v.model as 'Model', date_format(f.fuel_date, '%d.%m.%Y') as 'Datum tankování', f.fuel_amount, f.price_per_amount, fc.distance, f.price, concat(replace(format(f.tachometer, 0), ',', ' '), ' ') as 'tachometer' from refueling f inner join vehicles v on f.vehicle_id = v.id left join fuel_consumption fc on f.id = fc.refueling_id inner join brands b on v.brand = b.id where f.vehicle_id = ? ORDER BY f.add_date desc limit 1", array($carId));		
	
        $finalData = array();
        foreach($rawData as $data){
            $data[0] = Secure::encode($data[0]);
            $data[5] .= " l";           
            $data[6] .= " Kč";
            if($data[7] == 0){
                $data[7] = "Nejsou data";           
            } else {
                $data[7] .= " km";
            }
            $data[8] .= " Kč";
            $data[9] = User::getUserData()['id'];
            array_push($finalData, $data);
        }
        
        echo json_encode($finalData);
    }
        
    public function deleteFuel($id){
        echo json_encode(Db::send("delete from refueling where id = ?", array($id)));
    }
    
    private function getFuelType($carId){
        return Db::fetchOne("select fuel_type from vehicles where id = ?", array($carId))[0];
    }
        
    private function getDistance($car_id, $tachometer){
        $lastTacho = Db::fetchOne("select tachometer from refueling where vehicle_id = ? order by tachometer desc", array($car_id))[0];
        return $tachometer - $lastTacho;
    }
    
    private function getLastRefuel($carId){
        return Db::fetchOne("select id from refueling where vehicle_id = ? order by add_date desc", array($carId))[0];
    }
    
    private function calcFuelConsumption($driverId, $carId, $fuelDate, $distance, $fuelAmount){
        $id = $this->getLastRefuel($carId);
        $rawFuelConsumption = round($fuelAmount / $distance, 5);
        $realFuelConsumption = round($rawFuelConsumption * 100, 1);
        
        Db::send("insert into fuel_consumption (account_id, vehicle_id, fuel_consumption, distance, start_date, end_date, refueling_id) values (?, ?, ?, ?, ?, ?, ?)", array($driverId, $carId, $realFuelConsumption, $distance, $fuelDate, $fuelDate, $id));        
    }
}

