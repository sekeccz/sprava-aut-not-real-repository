<?php

class ShareManager {
    
    public function getSharedUsers(){
        $carIds = Car::getMyOwnCarsIds();
        $questionMarks = Car::getQuestionMarks($carIds);
        array_push($carIds, $_SESSION['userId']);
        $returnArray = Db::fetchAll("select a.username, a.nickname, uv.id, uv.account_id, uv.vehicle_id, b.name as 'brand', v.model from userXvehicles uv left join accounts a on uv.account_id = a.id inner join vehicles v on uv.vehicle_id = v.id inner join brands b on v.brand = b.id where uv.vehicle_id in ($questionMarks) and uv.account_id != ? order by uv.id asc", $carIds);
        for($i = 0; $i < count($returnArray); $i++){
            if(!empty($returnArray[$i]['nickname'])){
                $returnArray[$i]['username'] = $returnArray[$i]['nickname'];
            }
            if(empty($returnArray[$i]['username'])){
                $returnArray[$i]['username'] = "deletedUser";
            }
        }
        
        return $returnArray;
    }
    
    public function addShare($user, $carId){
        if(filter_var($user, FILTER_VALIDATE_EMAIL)) {
            $user = Db::fetchOne("select id from accounts where email = ?", array($user))[0];
        }
        $doExists = Db::check("select id from accounts where id = ?", array($user));
        if($doExists == 1){
            $isShared = Db::check("select id from userXvehicles where account_id = ? and vehicle_id = ?", array($user, $carId));
            if($isShared == 0){
                Db::send("insert into userXvehicles (account_id, vehicle_id) values (?, ?)", array($user, $carId));
                return 0; //success
            } else {
                return 2; //car is already assign
            }
        } else {
            return 1; //the user doesn't exits
        }
    }
    
    public function deleteShare($array){
        foreach($array as $id){
            Db::send("delete from userXvehicles where id = ?", array($id));
        }        
    }
}
