<?php

class Secure {
    
    public static function encode($input){
        return urlencode(openssl_encrypt($input,"AES-128-CTR", "-", 0, "-"));
    }
    
    public static function decode($input){
        return openssl_decrypt(urldecode($input),"AES-128-CTR", "-", 0, "-");
    }
}
