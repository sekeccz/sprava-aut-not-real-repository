<?php
class User {
    public static function getUserData(){
        if(!empty(Db::getUser())){
            return Db::getUser()[0];
        }        
    }
    
    public static function getUserName($id){
        if(!empty($data = Db::fetchOne("select username, nickname from accounts where id = ?", array($id)))){
            if($data['nickname'] == ""){
                return $data['username'];
            } else {
                return $data['nickname'];
            }
        }
    }
    
    public static function getAccounts(){
        return Db::fetchAll("select id, username, email from accounts");
    }
}
