<?php

class RideManager {

    public function getTable() {
        $fullArray = array();
        $carIds1 = Car::getMyCarsIds();
        $questionMarks1 = Car::getQuestionMarks($carIds1);
        for ($i = 0; $i < count($carIds1); $i++) {
            array_push($fullArray, $carIds1[$i]);
        }
        array_push($fullArray, User::getUserData()['id']);
        $carIds2 = Car::getMyOwnCarsIds();
        $questionMarks2 = Car::getQuestionMarks($carIds2);
        for ($i = 0; $i < count($carIds2); $i++) {
            array_push($fullArray, $carIds2[$i]);
        }
        
        $rawData = Db::fetchAll("select r.id, r.account_id, r.vehicle_id, r.tachometer_after, b.name as 'Brand', v.model as 'Model', date_format(r.ride_date, '%d.%m.%Y') as 'Datum jízdy', concat(replace(format(r.tachometer_before, 0), ',', ' '), ' ') as 'Km před', concat(replace(format(r.tachometer_after, 0), ',', ' '), '') as 'Km po', concat(replace(format(r.ride_km, 0), ',', ' '), ' km') as 'Ujetá vzdálenost', concat(replace(r.fuel_consumption, ',', ' '), '') as 'Spotřeba', v.fuel_type, r.tachometer_before, r.tachometer_after, r.ride_date, r.ride_km, r.fuel_consumption from rides r inner join vehicles v on r.vehicle_id = v.id inner join brands b on v.brand = b.id where (r.vehicle_id in ($questionMarks1) and r.account_id = ?) or r.vehicle_id in ($questionMarks2) ORDER BY r.ride_date desc, r.add_date desc, r.tachometer_after desc", $fullArray);
    
        $finalData = array();
        foreach($rawData as $data){
            $data[0] = Secure::encode($data[0]);
            $data[2] = Secure::encode($data[2]);
            if(is_null($data[7])){
                $data[7] = "Nejsou data";           
            }
            if(is_null($data[8])){
                $data[8] = "Nejsou data";           
            }
            if(is_null($data[10])){
                $data[10] = "Nejsou data";           
            } else {
                $data[10] .= " l/100km";
            }
            $data[17] = User::getUserData()['id'];
            $data[18] = User::getUserName($data[1]);
            array_push($finalData, $data);
        }
        
        echo json_encode($finalData);
    }
    
    public function getLastRide($carId){
        echo json_encode(Db::fetchAll("select tachometer_after from rides where vehicle_id = ? order by ride_date desc, tachometer_after desc", array($carId)));
    }

    public function addRide($driverId, $car, $kmBefore, $kmAfter, $driveKm, $driveDate, $fuelConsumption) {
        if ($driveKm == 0) {
            if($kmAfter < $kmBefore){
                $driveKm = 0;
            }
            $driveKm = $kmAfter - $kmBefore;
        }
        
        if($fuelConsumption == 0){
            $fuelConsumption = NULL;
        }
        
        if($kmBefore == 0){
            $kmBefore = NULL;
        }
        
        if($kmAfter == 0){
            $kmAfter = NULL;
        }

        Db::send("insert into rides (account_id, vehicle_id, tachometer_before, tachometer_after, ride_km, ride_date, fuel_consumption) values (?, ?, ?, ?, ?, ?, ?)", array($driverId, $car, $kmBefore, $kmAfter, $driveKm, $driveDate, $fuelConsumption));
    
        $rawData = Db::fetchAll("select r.id, r.account_id, r.vehicle_id, r.tachometer_after, b.name as 'Brand', v.model as 'Model', date_format(r.ride_date, '%d.%m.%Y') as 'Datum jízdy', concat(replace(format(r.tachometer_before, 0), ',', ' '), ' ') as 'Km před', concat(replace(format(r.tachometer_after, 0), ',', ' '), '') as 'Km po', concat(replace(format(r.ride_km, 0), ',', ' '), ' km') as 'Ujetá vzdálenost', concat(replace(r.fuel_consumption, ',', ' '), '') as 'Spotřeba', v.fuel_type, r.tachometer_before, r.tachometer_after, r.ride_date, r.ride_km, r.fuel_consumption from rides r inner join vehicles v on r.vehicle_id = v.id inner join brands b on v.brand = b.id where r.vehicle_id = ? ORDER BY r.add_date desc limit 1", array($car));
        $finalData = array();
        foreach($rawData as $data){
            $data[0] = Secure::encode($data[0]);
            $data[2] = Secure::encode($data[2]);
            if(is_null($data[7])){
                $data[7] = "Nejsou data";           
            }
            if(is_null($data[8])){
                $data[8] = "Nejsou data";           
            }
            if(is_null($data[10])){
                $data[10] = "Nejsou data";           
            } else {
                $data[10] .= " l/100km";
            }
            $data[17] = User::getUserData()['id'];
            array_push($finalData, $data);
        }
        
        echo json_encode($finalData);
    }

    public function updateRide($car, $kmBefore, $kmAfter, $driveKm, $driveDate, $fuelConsumption, $rideId) {
        if($fuelConsumption == 0){
            $fuelConsumption = NULL;
        }
        
        if($kmBefore == 0){
            $kmBefore = NULL;
        }
        
        if($kmAfter == 0){
            $kmAfter = NULL;
        }
        echo json_encode(Db::send("update rides set vehicle_id=?, tachometer_before=?, tachometer_after=?, ride_km=?, ride_date=?, fuel_consumption=? where id=?", array($car, $kmBefore, $kmAfter, $driveKm, $driveDate, $fuelConsumption, $rideId)));
    }

    public function deleteRide($id) {
        echo json_encode(Db::send("delete from rides where id = ?", array($id)));
    }
    
}
