<?php

class CalculatorManager {

    public function getTripCostData($carId){
        $past = 0;
        $found = 0;
        
        while($found == 0){
            $date = date('Y-m-d', strtotime("-". $past ." months"));
            $data = Db::fetchOne("select (select round(avg(f.fuel_consumption) * 1,1) from fuel_consumption f where f.vehicle_id = v.id and year(f.start_date) = :date and month(f.start_date) = month(:date)) as 'ffuel_consumption', (select F_PricePerKilometer(ffuel_consumption,(select avg(price_per_amount) from refueling where vehicle_id = :car and year(fuel_date) = :date and month(fuel_date) = month(:date)))) as 'PpK' from vehicles v where v.id = :car group by v.id", array(':date'=>$date, ':car'=>$carId));
            if($data[0] == NULL){
                $past++;
            } else {
                $found = 1;
            }
            if($past > 6){
                $found = 2;
            }
        }
        $data[2] = date_format(date_create($date), "m-Y");
        
        if($found != 1){
            $data[0] = NULL;
        }
        
        echo json_encode($data);
    }    

}
