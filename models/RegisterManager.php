<?php

class RegisterManager{
	
	public function register($userName, $email, $password, $rePassword){
            if(Db::check("select username from accounts where username = ?", array($userName)) > 0){
                return "register?error=3";
            }
            if(Db::check("select email from accounts where email = ?", array($email)) > 0){
                return "register?error=4";
            }
            if($password != $rePassword){
                return "register?error=1";
            }
            
            $id = $this->createNewId(10);
            $today = date('Y-m-d');
            if($id > 0){
                Db::send("insert into accounts (id, username, email, password, create_date, last_login_date, reset_password, email_verify) values (?, ?, ?, ?, ?, ?, '0', '0')", array($id, $userName, $email, $password, $today, $today));
                UserManager::sendVerifyEmail($id, $email);
                return "register?success=1";
            } else {
                return "register?error=2";
            }
            
	}
        
        public function Demo(){
            $id = $this->createNewId(10);
            Db::send("call P_create_demo(?)", array($id));
            $_SESSION['userId'] = $id;
            $_SESSION['enKey'] = "020123";
            return "homeboard";
        }
        
        private function createNewId($tries){
            if($tries == 0){
                return 0;
            }
            $id = rand(pow(10, 8-1), pow(10, 8)-1);
            if(Db::check("select id from accounts where id = ?", array($id) > 0)){
                $this->createNewId($tries-1);
            } else {
                return $id;
            }
        }
}