<?php
class Db{
    private static $connection;
    private static $settings = array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_EMULATE_PREPARES => true
    );
	
    public static function connect($host, $userName, $userPass, $dbName){
        if(!isset(self::$connection)){
            self::$connection = @new PDO(
            "mysql:host=$host;dbname=$dbName",
            $userName,
            $userPass,
            self::$settings
            );
        }
    }
	
    public static function fetchAll($query, $parameter = array()){
        self::connect("db.dw172.webglobe.com", "dolphinholder", "password", "spravaaut");
        $stmt = self::$connection->prepare($query);
        $stmt->execute($parameter);
        return $stmt->fetchAll();
    }
	
    public static function fetchOne($query, $parameter = array()){
        self::connect("db.dw172.webglobe.com", "dolphinholder", "password", "spravaaut");
        $stmt = self::$connection->prepare($query);
        $stmt->execute($parameter);
        $res = $stmt->fetchAll();
        return $res[0];
    }
        
    public static function getUser(){
        $finalArray = array();
        if(isset($_SESSION['userId']) and strlen($_SESSION['userId']) == 8){
            $id = $_SESSION['userId'];
            self::connect("db.dw172.webglobe.com", "dolphinholder", "password", "spravaaut");
            $user = self::$connection->prepare("select a.id, a.nickname, a.username, a.email, a.password, a.create_date, a.last_login_date, a.reset_password, a.email_verify, a.demo, count(v.id) as vehicle_count from accounts a left join vehicles v on a.id = v.account_id where a.id = ?");
            $user->execute([$id]);
            if($user->rowCount() > 0){
                array_push($finalArray, $user->fetch());
                $finalArray[0]['real_account']  = true; 
                $cars = self::$connection->prepare("select uv.vehicle_id, uv.is_owner, v.archive from userXvehicles uv left join vehicles v on uv.vehicle_id = v.id where uv.account_id = ?");
                $cars->execute([$id]);
                array_push($finalArray, $cars->fetchAll());
                $settings = self::$connection->prepare("select settings from userXsettings where account_id = ?");
                $settings->execute([$id]);
                array_push($finalArray, $settings->fetch());
                               
                //echo "<pre>"; print_r($finalArray); echo "</pre>";
                return $finalArray;
            }
        } else if(isset($_SESSION['userId'])) {
            $finalArray = array(array('id' => 0, 'nickname' => "", 'username' => "Ghost", 'email' => "iamghost@sprava-aut.cz", 'password' => "", 'create_date' => "23-01-2020", 'last_login_date' => "23-01-2020", 'reset_password' => 0, 'email_verify' => 1, 'demo' => 0, 'vehicle_count' => 1, 'real_account' => 0), array(array(0 => $_SESSION['userId'], 1 => 1, 2 => 0, 'vehicle_id' => $_SESSION['userId'], 'is_owner' => 1, 'archive' => 0)), array('settings' => "1011K11K0K"));                                                                                                                               
            //echo "<pre>"; print_r($finalArray); echo "</pre>";
            return $finalArray;            
        }
    }
	
    public static function logIn($query, $parameter){
	self::connect("db.dw172.webglobe.com", "dolphinholder", "password", "spravaaut");
	$login = self::$connection->prepare($query);
	$login->execute($parameter);
	if($login->rowCount() > 0){
            $ret[0] = 1;
            $ret[1] = $login->fetch()[0];
            return $ret;
	} else {
            $ret[0] = 0;
            return $ret;
	}		
    }
	
    public static function send($query, $parameter = array()){
	self::connect("db.dw172.webglobe.com", "dolphinholder", "password", "spravaaut");
	$send = self::$connection->prepare($query);
	$send->execute($parameter);
    }
	
    public static function check($query, $parameter = array()){
        self::connect("db.dw172.webglobe.com", "dolphinholder", "password", "spravaaut");
        $check = self::$connection->prepare($query);
	$check->execute($parameter);
        return $check->rowCount();
    }
}