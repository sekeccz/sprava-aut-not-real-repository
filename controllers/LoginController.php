<?php

class LoginController extends Controller{

	public function processURL($parameter){
		$this->header['headline'] = "Správa Aut - Přihlášení";
		
		if(isset($_POST['userName'])){
			$log = new LoginManager();
			$this->redirectTo($log->logIn($_POST['userName'], md5($_POST['password']), $_POST['srcPage']));
		}
                
                $this->redirectTo("error404");
	}
}