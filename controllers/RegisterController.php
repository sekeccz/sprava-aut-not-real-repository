<?php

class RegisterController extends Controller{
    
	public function processURL($parameter){
            
		if(!empty(User::getUserData())){
			$this->redirectTo('homeboard');
		}		
		$this->header['headline'] = "Správa Aut - Registrace";
		$this->header['page'] = "register";
                
                if(isset($_POST['username'])){
			$reg = new RegisterManager();
			$this->redirectTo($reg->register($_POST['username'], $_POST['email'], md5($_POST['password']), md5($_POST['repassword'])));
		}
                
		$this->view = "register";
	}
}