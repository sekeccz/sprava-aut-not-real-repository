<?php
class FaqController extends Controller{
	public function processURL($parameter){
		if(!empty(User::getUserData())){
			$this->redirectTo('homeboard');
		}	
		$this->header['headline'] = "Správa Aut - Faq";
		$this->header['page'] = "faq";
		$this->view = "faq";
	}
}