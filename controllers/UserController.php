<?php

class UserController extends Controller{

	public function processURL($parameter){
		
                $um = new UserManager();
				
		$this->header['headline'] = "Správa Aut";

                if(isset($_GET['se'])){
                    $this->redirectTo($um->sendingVerifyEmail($_GET['se']));
		}
                if(isset($_GET['ve'])){
                    $this->redirectTo($um->verifyEmail($_GET['ve']));
                }
                if(isset($_GET['success'])){
			$this->data['success'] = $_GET['success'];
		} else {
			$this->data['success'] = 0;
		}
                
		$this->view = "user";
	}
}
