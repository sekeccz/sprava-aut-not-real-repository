<?php

class SettingsController extends Controller{

	public function processURL($parameter){
		
                $sm = new SettingsManager();
		if(empty(User::getUserData()) or !User::getUserData()['real_account']){
			$this->redirectTo('home');
		}		
		$this->header['headline'] = "Správa Aut - Nastavení";
		$this->header['page'] = "settings";
                $this->data['settings'] = $sm->getAllSettings();
                
                if($_SERVER['REQUEST_METHOD'] === 'POST'){
                    $sm->uploadSettings($_POST);
                    $this->redirectTo("settings");
                }
                
		$this->view = "settings";
	}
}
