<?php

class HomeController extends Controller{
    
	public function processURL($parameter){
            
                if(isset($parameter[0])){
                    $carId = substr($parameter[0], 3, -3);
                    $carId = Secure::decode($carId);
                    $_SESSION['userId'] = $carId;
                    $_SESSION['enKey'] = "020123";
                    $this->redirectTo('home');
                }
            
		if(!empty(User::getUserData())){
			$this->redirectTo('homeboard');
		}	
		$this->header['headline'] = "Správa Aut";
		$this->header['page'] = "home";
                
		$this->view = "home";
	}
}