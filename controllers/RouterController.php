<?php
class RouterController extends Controller{
	protected $controller;
	
	public function processURL($parameter){
		$parsedURL = $this->parseURL($parameter[0]);
		
		if(empty($parsedURL[0])){
			$this->redirectTo('home');
		}
		
		$controllerClass = $this->toCamelNotation(array_shift($parsedURL)) . 'Controller';
		
		if(file_exists('controllers/' . $controllerClass . '.php')){
			$this->controller = new $controllerClass;
		} else {
			$this->redirectTo('error404');
		}
		
		$this->controller->processURL($parsedURL);
		
		$this->data['headline'] = $this->controller->header['headline'];
		$this->data['keywords'] = $this->controller->header['keywords'];
		$this->data['description'] = $this->controller->header['description'];
		$this->data['page'] = $this->controller->header['page'];
		$this->data['userData'] = User::getUserData();
		
		$this->view = 'main';
	}
	
	private function parseURL($url){
		$parsedURL = parse_url($url);
		$parsedURL['path'] = ltrim($parsedURL['path'], "/");
		$parsedURL['path'] = trim($parsedURL['path']);
		$res = explode("/", $parsedURL['path']);	
		return $res;
	}
	
	private function toCamelNotation($string){
		$res = str_replace('-', ' ', $string);
		$res = ucwords($res);
		$res = str_replace(' ', '', $res);
		return $res;
	}
}