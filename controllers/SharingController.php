<?php

class SharingController extends Controller{

	public function processURL($parameter){
		
                $sm = new ShareManager();
		if(empty(Db::checkLogin())){
			$this->redirectTo('home');
		}		
		$this->header['headline'] = "Správa Aut - Půjčování";
		$this->header['page'] = "sharing";
                $this->data['shareData'] = $sm->getSharedUsers();
		$this->data['carList'] = Car::getMyCars(Car::getMyOwnCarsIds());
                
                if($_SERVER['REQUEST_METHOD'] == "POST"){
                    switch($sm->addShare($_POST['user'], $_POST['car'])){
                        case 0:
                            $this->redirectTo("sharing");
                            break;
                        case 1:
                            $this->redirectTo("sharing?error=1");
                            break;
                        case 2:
                            $this->redirectTo("sharing?error=2");
                            break;
                    }
                }
                
                if(isset($_GET['ids'])){
                    $sm->deleteShare($_GET['ids']);
                    $this->redirectTo("sharing");
                }
                
		$this->view = "sharing";
	}
}
