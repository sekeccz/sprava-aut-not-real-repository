<?php
class DemoController extends Controller{
	public function processURL($parameter){
		if(!empty(User::getUserData())){
			$this->redirectTo('homeboard');
		}	
		$this->header['headline'] = "Správa Aut - Demo";
		$this->header['page'] = "demo";
                
                if(isset($_GET['create'])){
                    $reg = new RegisterManager();
                    $this->redirectTo($reg->demo());
                }
                
		$this->view = "demo";
	}
}