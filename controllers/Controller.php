<?php

abstract class Controller{

	protected $data = array();

	protected $view = "";

	protected $header = array(

		'headline' => '',

		'keywords' => '',

		'description' => '',
		
		'page' => ''

	);

	

	abstract function processURL($parameter);

	

	public function renderView(){

		if($this->view){

			extract($this->data);

			require("views/" . $this->view .".phtml");

		}

	}

	

	public function redirectTo($url){
                
		header("Location: /$url");

		header("Connection: close");

		exit;

	}

}