<?php

class RideController extends Controller{

	public function processURL($parameter){
		
		if(empty($userData = User::getUserData())){
			$this->redirectTo('home');
		}		
		$this->header['headline'] = "Správa Aut - Jízdy";
		$this->header['page'] = "ride";
		$this->data['carList'] = Car::getMyCars(Car::getMyActiveCarsIds());
                $this->data['userData'] = $userData;
		
		$this->view = "ride";                
	}
}