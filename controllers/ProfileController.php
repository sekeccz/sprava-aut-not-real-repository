<?php

class ProfileController extends Controller{

	public function processURL($parameter){
		
                $pm = new ProfileManager();
                $lm = new LogoutManager();
		if(empty($userData = User::getUserData())){
			$this->redirectTo('home');
		}		
		$this->header['headline'] = "Správa Aut - Profil";
		$this->header['page'] = "profile";
                $this->data['userData'] = $userData;
                $this->data['carData'] = $pm->getCars(Car::getMyCarsIds());
		$this->data['carList'] = Car::getCarBrands();
                $this->data['fuelList'] = Car::getFuelList();
                if($userData['nickname'] == ""){
                    $this->data['nickname'] = "nenastaveno";
                } else {
                    $this->data['nickname'] = $userData['nickname'];
                }
                
                if(isset($_POST['thing'])){
                    switch($_POST['thing']) {
                        case "nickname":
                            $pm->updateNickname($_POST['e_thing'], $userData['id']);
                            $this->redirectTo("profile");
                            break;
                        case "email":
                            $pm->updateEmail($_POST['e_thing'], $userData['id']);
                            $this->redirectTo("profile");
                            break;
                    }
                }
                if(isset($_POST['car_id'])){
                    Car::deleteCar(Secure::decode($_POST['car_id']));
                    $this->redirectTo('profile');
                }
                if(isset($_POST['button']) && $_POST['button'] == "Přidat"){
                    $this->redirectTo($pm->createCar($userData['id'], $_POST['brand'], $_POST['model'], $_POST['year'], $_POST['engine'], $_POST['kw_power'], $_POST['fuel_type']));
                }
                
                if(isset($_POST['button']) && $_POST['button'] == "Smazat"){
                    $this->redirectTo($lm->deleteUser(Secure::decode($_POST['id'])));
                }
                
                if(isset($_GET['edit'])){
			$this->data['edit'] = $_GET['edit'];
		} else {
			$this->data['edit'] = 0;
		}
                if(isset($_GET['cadd'])){
			$this->data['cadd'] = $_GET['cadd'];
		} else {
			$this->data['cadd'] = 0;
		}
                if(isset($_GET['success'])){
			$this->data['success'] = $_GET['success'];
		} else {
			$this->data['success'] = 0;
		}
                
		$this->view = "profile";
	}
}