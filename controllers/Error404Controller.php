<?php
class Error404Controller extends Controller{
	
	public function processURL($param){
		header("HTTP/1.0 404 Not Found");
		$this->header['headline'] = "Chyba 404";
		$this->view = "error404";
	}
}