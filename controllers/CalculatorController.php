<?php
class CalculatorController extends Controller{
    
	public function processURL($parameter){
            
                if(empty($userData = User::getUserData()) or !User::getUserData()['real_account']){
			$this->redirectTo('homeboard');
                }
		$this->header['headline'] = "Správa Aut - Kalkulačka";
		$this->header['page'] = "calculator";
                $this->data['carList'] = Car::getMyCars(Car::getMyActiveCarsIds());
                $this->data['userData'] = $userData;
                
                               
		$this->view = "calculator";
	}
}
