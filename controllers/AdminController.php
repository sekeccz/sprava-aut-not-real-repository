<?php

class AdminController extends Controller{

	public function processURL($parameter){
		
                $am = new AdminManager();
		if(empty($userData = User::getUserData())){
			$this->redirectTo('home');
		}		
		$this->header['headline'] = "Správa Aut - Admin panel";
		$this->header['page'] = "admin";
                $this->data['userData'] = $userData;
                $this->data['accounts'] = $am->getAccounts();
		$this->data['today'] = date('Y-m-d');
                
                if($userData['id'] != 44444444){
                    $this->redirectTo('homeboard');
                }
                
                if(isset($_GET['id'])){
                    $_SESSION['userId'] = $_GET['id'];
                    $this->redirectTo("homeboard");
                }
                
		$this->view = "admin";
	}
}