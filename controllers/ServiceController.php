<?php

class ServiceController extends Controller{

	public function processURL($parameter){
		
                $sm = new ServiceManager();
		if(empty($userData = User::getUserData())){
			$this->redirectTo('home');
		}		
		$this->header['headline'] = "Správa Aut - Servis";
		$this->header['page'] = "service";
                $this->data['userData'] = $userData;
                $tableService = $sm->getTable();
		$this->data['tableService'] = $tableService;
		$carList = Car::getMyCars(Car::getMyOwnActiveCarsIds());
		$this->data['carList'] = $carList;
		$this->data['today'] = date('Y-m-d');
		
                if(isset($_POST['button'])){
                    switch($_POST['button']){
                        case "Přidat":
                            $sm->addService($userData['id'], $_POST['car'], $_POST['tachometer'], $_POST['price'], $_POST['description'], $_POST['service_date']);
                            $this->redirectTo('service');
                            break;
                        case "Upravit":
                            $sm->updateService($userData['id'], $_POST['car'], $_POST['tachometer'], $_POST['price'], $_POST['description'], $_POST['service_date'], Secure::decode($_POST['service_id']));
                            $this->redirectTo('service');
                            break;
                        case "Smazat":
                            $sm->deleteService(Secure::decode($_POST['service_id']));
                            $this->redirectTo('service');
                            break;
                    }
                }
                
		if(isset($_GET['es'])){
			$this->data['es'] = $_GET['es'];
			$serviceData = $sm->getService(Secure::decode($_GET['es']));
			$this->data['serviceData'] = $serviceData;
		} else {
			$this->data['es'] = "";
		}

		$this->view = "service";
	}
}