<?php

class LogoutController extends Controller{

	public function processURL($parameter){
		$this->header['headline'] = "Správa Aut - Odhlášení";
		
                $log = new LogoutManager();
                if(isset($_POST['id'])){
                    $this->redirectTo($log->deleteUser(Secure::decode($_POST['id'])));
                } else {
                    if(isset($_SESSION['userId'])){
                        $this->redirectTo($log->logOut());
                    } else {
                        $this->redirectTo("error404");
                    }
                }
	}
}