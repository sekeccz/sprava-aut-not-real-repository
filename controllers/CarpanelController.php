<?php
class CarpanelController extends Controller{
    
	public function processURL($parameter){
            
                $cp = new CarpanelManager();
		if(empty($userData = User::getUserData()) or !User::getUserData()['real_account']){
			$this->redirectTo('homeboard');
                }
		$this->header['headline'] = "Správa Aut - Nastavení auta";
		$this->header['page'] = "carpanel";
                $this->data['carList'] = $cp->getCarList();
                $this->data['userData'] = $userData;
                $this->data['accountList'] = User::getAccounts();

                if(isset($_POST['deleteCarId'])){
                    Car::deleteCar(Secure::decode($_POST['deleteCarId']));
                }
                               
		$this->view = "carpanel";
	}
}
