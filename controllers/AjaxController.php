<?php

class AjaxController extends Controller{

    public function processURL($parameter){
		
        $am = new AjaxManager();        
        $rm = new RideManager();
        
        $fm = new FuelManager();
        $cpm = new CarpanelManager();
        $cm = new CalculatorManager();
        $sm = new SettingsManager();
        if(empty($userData = User::getUserData())){
            $this->redirectTo('home');
	}
	$today = date('Y-m-d');
		
        //wtd => what to do        
        switch($parameter[0]){
            //Ride
            case "addRide":
                $rm->addRide($userData['id'], Secure::decode($_POST['iCarId']), $_POST['iKmBefore'], $_POST['iKmAfter'], $_POST['iKm'], $_POST['iRideDate'], $_POST['iFuel']);
                die;
                break;
            case "editRide":
                $rm->updateRide(Secure::decode($_POST[0]), $_POST[1], $_POST[2], $_POST[3], $_POST[4], $_POST[5], Secure::decode($_POST[6]));
                die;
                break;
            case "deleteRide":
                $rm->deleteRide(Secure::decode($_POST['iDeleteRide']));
                die;
                break;
            case "getRides":
                $rm->getTable();
                die;
                break;
            case "getLastRide":
                $rm->getLastRide(Secure::decode($_POST['iCarId']));
                die;
                break;
                
            //Fuel
            case "addFuel":
                $fm->addFuel($userData['id'], Secure::decode($_POST[0]), $_POST[1], $_POST[2], $_POST[3], $_POST[4], $_POST[5], $_POST[6]);
                die;
                break;
            case "getFuel":
                $fm->getTable();
                die;
                break;
            case "deleteFuel":
                $fm->deleteFuel(Secure::decode($_POST[0]));
                die;
                break;
            
            //CarPanel
            case "getCPData":
                echo json_encode($cpm->dataRequest($_POST['iCar']));
                die;
                break;
            case "createShare":
                echo json_encode($cpm->createShare($_POST['iShareAddUser'], $_POST['iShareAddId']));
                die;
                break;
            case "createLink":
                echo json_encode($cpm->createLink($_POST['iShareCar']));
                die;
                break;
            case "giveCar":
                echo json_encode($cpm->giveCar($_POST['iGiveUser'], $_POST['iGiveCarId']));
                die;
            case "deleteShare":
                echo json_encode($cpm->deleteShare($_POST['iShareDelete']));
                die;
                break;
            case "changeCarInfo":
                echo json_encode($cpm->editCar($_POST['iCarId'], $_POST['iMfgdt'], $_POST['iEngine'], $_POST['iPower']));
                die;
                break;
            case "archiveCar":
                echo json_encode($cpm->archiveCar($_POST['iArchiveCarId']));
                die;
                break;
            
            //Calculator
            case "getTripCostData":
                $cm->getTripCostData(Secure::decode($_POST[0]));
                die;
                break;
                
            //Main
            case "getSettings":
                echo json_encode($sm->getAllSettings());
                die;
                break;
        }
    }
}