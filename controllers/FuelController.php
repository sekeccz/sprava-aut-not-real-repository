<?php

class FuelController extends Controller{

	public function processURL($parameter){
		
		if(empty($userData = User::getUserData())){
			$this->redirectTo('home');
		}		
		$this->header['headline'] = "Správa Aut - Tankování";
		$this->header['page'] = "fuel";
                $this->data['userData'] = $userData;
		$carList = Car::getMyCars(Car::getMyActiveCarsIds());
		$this->data['carList'] = $carList;
                
		$this->view = "fuel";
	}
}